with open("books/frankenstein.txt") as f:
    file_contents = f.read()
    words = file_contents.split()
    print("--- Begin report of books/frankenstein.txt ---")
    print(f"{len(words)} words found in the document")
    print("")

    letters_count = {}
    for word in words:
        for letter in word:
            if letter.isalpha():
                letters_count[letter.lower()] = letters_count.get(
                    letter.lower(), 0) + 1

    lst = sorted(letters_count.items(), key=lambda x: x[1], reverse=True)
    for count in lst:
        print(f"The '{count[0]}' character was found {count[1]} times")

    print("--- End report ---")
